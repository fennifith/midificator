export { default as MidiEvent } from './components/MidiEvent.vue';
export { default as Dashboard } from './components/Dashboard.vue';
export { default as App } from './components/App.vue';
export * from './hooks';
