import { defineStore } from "pinia";
import { parseMidi, parseMidis } from "../utils/parseMidi";
import { TimeUnit, fps, unitsToFrames } from "vue-three-js";
import { v4 as uuidv4 } from 'uuid';

export const useTrackStore = defineStore('tracks', {
	state: () => ({
		tracks: [] as MidiTrack[],
	}),
	getters: {
		lastFrame: (state) => {
			return Math.max(0, ...state.tracks.map(t => t.midiFrames || 0));
		},
	},
	actions: {
		async addTrack(t: MidiTrackInfo) {
			const track = await processMidi(t);
			this.tracks = [...this.tracks, track];
		},
		async removeTrack(id: string) {
			this.tracks = this.tracks.filter(t => t.id !== id);
		},
	}
});

export type MidiTrackInfo = {
	name: string;
	midi: string[];
	padStart?: TimeUnit;
	padEnd?: TimeUnit;
	audio: string;
	audioStart?: TimeUnit;
};

export type MidiTrack = {
	id: string;
	name: string;
	midi: Awaited<ReturnType<typeof parseMidi>>;
	midiFrames: number;
	padStart: number;
	padEnd: number;
	audio?: HTMLAudioElement;
	audioStart: number;
	audioDuration: number;
	audioFft: {
		byteFrequencyData: Uint8Array[],
		byteTimeDomainData: Uint8Array[],
		frequencyBinCount: number,
	};
};

export async function processMidi(t: MidiTrackInfo): Promise<MidiTrack> {
	const midi = (await parseMidis(t.midi)).flat();
	const padStart = unitsToFrames(t.padStart || 0);
	const padEnd = unitsToFrames(t.padEnd || 0);
	const midiFrames = padStart + Math.max(...midi.flat().map(m => m.lengthFrames)) + padEnd;

	return {
		id: uuidv4(),
		name: t.name,
		midi,
		padStart,
		padEnd,
		midiFrames,
		audio: new Audio(t.audio),
		audioStart: padStart + unitsToFrames(t.audioStart || 0),
		...(await processWaveform(t)),
	};
}

export async function processWaveform(t: MidiTrackInfo): Promise<Pick<MidiTrack, "audioFft" | "audioDuration">> {
	if (!t.audio) throw "Error: No audio to process!";

	const audioCtx1 = new AudioContext();
	const arrayBuffer = await fetch(t.audio).then(r => r.arrayBuffer());
	const audioBuffer1 = await audioCtx1.decodeAudioData(arrayBuffer);

	const audioCtx = new OfflineAudioContext({
		sampleRate: audioBuffer1.sampleRate,
		length: audioBuffer1.length,
		numberOfChannels: audioBuffer1.numberOfChannels,
	});

	const analyzer = audioCtx.createAnalyser();
	analyzer.fftSize = 2048;

	// const audioBuffer = await audioCtx.decodeAudioData(arrayBuffer);
	const source = audioCtx.createBufferSource();
	source.buffer = audioBuffer1;

	source.connect(analyzer);
	source.connect(audioCtx.destination);
	source.start();

	const frames = Math.ceil(unitsToFrames(audioBuffer1.duration + "s"));
	const byteFrequencyData: Uint8Array[] = Array(frames);
	const byteTimeDomainData: Uint8Array[] = Array(frames);

	for (let i = 0; i < frames; i++) {
		const index = i;
		byteFrequencyData[index] = new Uint8Array(analyzer.frequencyBinCount);
		byteTimeDomainData[index] = new Uint8Array(analyzer.frequencyBinCount);

		audioCtx.suspend(index / fps).then(() => {
			analyzer.getByteFrequencyData(byteFrequencyData[index]);
			analyzer.getByteTimeDomainData(byteTimeDomainData[index]);
			return audioCtx.resume();
		});
	}

	await audioCtx.startRendering();
	return {
		audioFft: {
			byteFrequencyData,
			byteTimeDomainData,
			frequencyBinCount: analyzer.frequencyBinCount,
		},
		audioDuration: frames,
	};
}
