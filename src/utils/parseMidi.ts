import midi from 'midi-file';
import { MidiEventInfo } from "../hooks";
import { fps } from 'vue-three-js';

function basename(str: string) {
	return str.split("/").at(-1)?.split(".")[0];
}

export type MidiTrackEvents = {
	name: string;
	events: MidiEventInfo[];
	lengthTicks: number;
	lengthFrames: number;
};

export async function parseMidi(path: string) {
	const ret = [] as MidiTrackEvents[];

	const buffer = await fetch(path).then(r => r.arrayBuffer());
	const { header, tracks } = midi.parseMidi(new Uint8Array(buffer));

	// default beats per second = 2 (120 BPM)
	let beatsPerSecond = 2;
	let beatsPerSecondDefault = true;

	// if constant ticksPerFrame specified, use that - otherwise, rely on beatsPerSecond
	const ticksPerSecond = () => header.ticksPerFrame && header.framesPerSecond
		? header.ticksPerFrame * header.framesPerSecond
		: (header.ticksPerBeat || 256) * beatsPerSecond;

	const ticksPerRenderFrame = () => ticksPerSecond() / fps;

	tracks.forEach((track, index) => {
		const name = `${basename(path)}${index}`;
		const events: MidiEventInfo[] = [];
		// tick starts at 0 on each track
		let tick = 0;

		const lastNotes = new Map<number, MidiEventInfo>();

		for (const event of track) {
			// add the deltaTime of each event processed
			tick += event.deltaTime;

			if (event.type === "setTempo" && beatsPerSecondDefault) {
				beatsPerSecond = 1 / (event.microsecondsPerBeat * 1e-6);
				beatsPerSecondDefault = false;
			}

			const eventFrame = Math.round(tick / ticksPerRenderFrame());

			if (event.type === "noteOn") {
				const eventInfo = {
					name,
					frame: eventFrame,
					value: event.noteNumber,
					intensity: event.velocity,
				};

				lastNotes.set(eventInfo.value, eventInfo);
				events.push(eventInfo);
			}

			if (event.type === "noteOff") {
				const eventInfo = lastNotes.get(event.noteNumber);
				if (eventInfo != null) {
					eventInfo.duration = eventFrame - eventInfo.frame;
					lastNotes.delete(event.noteNumber);
				}
			}
		}

		ret.push({
			name,
			events,
			lengthTicks: tick,
			lengthFrames: Math.round(tick / ticksPerRenderFrame()),
		});
	});

	return ret;
}

export async function parseMidis(paths: string[]) {
	return Promise.all(
		paths.map((path) => parseMidi(path))
	);
}
