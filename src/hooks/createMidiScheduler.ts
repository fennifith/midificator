import { InjectionKey, computed, provide } from "vue";
import { useTrackStore } from "../stores/tracks";

export type MidiEventInfo = {
	name: string;
	frame: number;
	duration?: number;
	value: number;
	intensity: number;
};

export type MidiEventListener = {
	name: string;
	callback: (e: MidiEventInfo) => void | Promise<void>;
	offset?: number;
};

export function createMidiScheduler() {
	const events = computed(() => {
		const trackStore = useTrackStore();

		const events = new Map<number, MidiEventInfo[]>();

		for (const track of trackStore.tracks) {
			for (const midi of track.midi) {
				for (const event of midi.events) {
					const eventFrame = track.padStart + event.frame;

					let eventArr = events.get(eventFrame);
					if (!eventArr) {
						eventArr = [];
						events.set(eventFrame, eventArr);
					}

					eventArr.push(event);
				}
			}
		}

		return events;
	});

	let listeners: MidiEventListener[] = [];

	/**
	 * This should rarely be used, as it disregards the listener offset.
	 */
	async function dispatchEvent(event: MidiEventInfo) {
		await Promise.all(
			listeners
				.filter(listener => event.name === listener.name)
				.map(listener => listener.callback(event))
				.concat(
					listeners
						.filter(listener => listener.name === "*")
						.map(listener => listener.callback(event))
				)
		);
	}

	async function dispatchFrame(frame: number) {
		const frameEvents = events.value.get(frame) || [];

		await Promise.all(
			listeners.filter(l => l.offset === undefined || l.offset === 0)
				.flatMap(listener => {
					frameEvents.filter(e => listener.name === e.name || listener.name === "*")
						.map(listener.callback)
				})
		);

		await Promise.all(
			listeners.filter(l => l.offset !== undefined && l.offset !== 0)
				.flatMap(listener => {
					// find events for current frame + listener offset
					const listenerEvents = events.value.get(frame + (listener.offset || 0));
					if (!listenerEvents) return [];

					return listenerEvents
						.filter(e => listener.name === e.name || listener.name === "*")
						.map(listener.callback);
				})
		);
	}

	function addEventListener(name: string, callback: MidiEventListener["callback"], offset?: number) {
		listeners.push({ name, callback, offset });

		return {
			remove() { removeEventListener(callback); }
		};
	}

	function removeEventListener(callback: MidiEventListener["callback"]) {
		listeners = listeners.filter((l) => l.callback !== callback);
	}

	const scheduler = {
		dispatchEvent, dispatchFrame,
		addEventListener, removeEventListener,
	};

	provide(EventSchedulerSymbol, scheduler);
	return scheduler;
}

export type EventScheduler = ReturnType<typeof createMidiScheduler>;

export const EventSchedulerSymbol: InjectionKey<EventScheduler> = Symbol();
