import { inject } from "vue";
import { EventSchedulerSymbol } from "./createMidiScheduler";

export function useMidiScheduler() {
	const scheduler = inject(EventSchedulerSymbol);
	if (!scheduler) throw "Scheduler instance not found! Are you sure this is inside a <MidiProvider>?";
	return scheduler;
}
