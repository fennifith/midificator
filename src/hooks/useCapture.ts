import { currentFrame, fps, onAfterRender, onRenderStop, runBlocking, waitBlocking } from "vue-three-js";
import { Muxer, FileSystemWritableFileStreamTarget } from 'webm-muxer';
import { saveAs } from "file-saver";
import { useTrackStore } from "../stores/tracks";

const AUDIO_SAMPLE_RATE = 48000;
const AUDIO_NUM_CHANNELS = 2;

export function useCapture() {
	let fileHandle: FileSystemFileHandle;
	let fileStream: FileSystemWritableFileStream;
	let muxer: Muxer<FileSystemWritableFileStreamTarget>;

	const videoEnc = new VideoEncoder({
		output: (chunk, meta) => {
			muxer.addVideoChunk(chunk, meta as any, chunk.timestamp);
		},
		error: console.error,
	});

	videoEnc.configure({
		codec: "vp09.00.10.08",
		width: 1920,
		height: 1080,
		bitrate: 8000 * 1000, // 8000 kbps
		framerate: 60,
	});

	// TODO: types for web AudioEncoder API
	const AudioEncoder = (window as any).AudioEncoder;
	const audioEnc = new AudioEncoder({
		output: (chunk: any, meta: any) => {
			muxer.addAudioChunk(chunk, meta as any);
		},
		error: console.error,
	});

	audioEnc.configure({
		codec: 'opus',
		numberOfChannels: AUDIO_NUM_CHANNELS,
		sampleRate: AUDIO_SAMPLE_RATE,
		bitrate: 128000,
	});

	const trackStore = useTrackStore();

	// set up the muxer / output stream
	runBlocking(async () => {
		const dir = await navigator.storage.getDirectory();
		fileHandle = await dir.getFileHandle("render.webm", { create: true });
		fileStream = await fileHandle.createWritable({
			keepExistingData: false,
		});

		muxer = new Muxer({
			target: new FileSystemWritableFileStreamTarget(fileStream),
			video: {
				codec: "V_VP9",
				width: 1920,
				height: 1080,
				frameRate: fps,
			},
			audio: {
				codec: "A_OPUS",
				sampleRate: AUDIO_SAMPLE_RATE,
				numberOfChannels: AUDIO_NUM_CHANNELS,
			},
			firstTimestampBehavior: "offset",
		});

		// then, fetch and encode the audio tracks
		const audioCtx = new AudioContext({
			sampleRate: AUDIO_SAMPLE_RATE,
		});
		const audioBuffers = await Promise.all(trackStore.tracks.map(async t => {
			if (t.audio) {
				const arrayBuffer = await fetch(t.audio.src).then(r => r.arrayBuffer());
				const audioBuffer = await audioCtx.decodeAudioData(arrayBuffer);
				return audioBuffer;
			}
		}));

		/*const audioCtx = new OfflineAudioContext({
			sampleRate: AUDIO_SAMPLE_RATE,
			length: Math.max(...audioBuffers.map(b => b?.length || 0)),
			numberOfChannels: 2,
		});*/

		const audioMaxLen = Math.max(...audioBuffers.map((b, i) => {
			if (b) {
				const track = trackStore.tracks[i];
				return (track.audioStart / fps) * AUDIO_SAMPLE_RATE + b.length;
			} else return 0;
		}));
		const audioF32Arr = new Float32Array(audioMaxLen * AUDIO_NUM_CHANNELS);

		for (let i = 0; i < audioBuffers.length; i++) {
			const audioBuffer = audioBuffers[i];
			if (!audioBuffer) continue;

			const track = trackStore.tracks[i];

			const startOffset = (track.audioStart / fps) * AUDIO_SAMPLE_RATE;

			for (let channel = 0; channel < AUDIO_NUM_CHANNELS; channel++) {
				let channelOffset = channel * audioMaxLen;
				audioF32Arr.set(
					audioBuffer.getChannelData(channel % audioBuffer.numberOfChannels),
					Math.floor(channelOffset + startOffset),
				);
			}
		}

		// TODO: types for web AudioData API
		const AudioData = (window as any).AudioData;
		const audioData = new AudioData({
			format: "f32-planar",
			sampleRate: AUDIO_SAMPLE_RATE,
			numberOfChannels: AUDIO_NUM_CHANNELS,
			numberOfFrames: audioF32Arr.length / AUDIO_NUM_CHANNELS,
			timestamp: 0,
			data: audioF32Arr,
		})

		audioEnc.encode(audioData);

		/*const mix = audioCtx.createMediaStreamDestination();

		for (let i = 0; i < audioBuffers.length; i++) {
			const audioBuffer = audioBuffers[i];
			if (!audioBuffer) continue;

			const track = trackStore.tracks[i];

			const audioSource = audioCtx.createBufferSource();
			audioSource.buffer = audioBuffer;
			audioSource.connect(mix);

			audioSource.start(track.audioStart / fps);

			// TODO: types for web MediaStreamTrackProcessor API
			const MediaStreamTrackProcessor = (window as any).MediaStreamTrackProcessor;
			const trackProcessor = new MediaStreamTrackProcessor({
				track: mix.stream.getAudioTracks()[0],
			});

			const audioStream = new WritableStream({
				write(audioData) {
					audioEnc.encode(audioData);
					audioData.close();
				}
			});
			trackProcessor.readable.pipeTo(audioStream);
		}*/
	});

	let startFrame = currentFrame.value;
	onAfterRender(async (frame) => {
		let currentFrame = frame - startFrame;

		const canvas = document.getElementsByTagName("canvas")[0];
		const videoFrame = new VideoFrame(canvas, {
			// calculate timestamp in microseconds
			timestamp: Math.floor((currentFrame / 60) * 1000000)
		});

		videoEnc.encode(videoFrame, { keyFrame: currentFrame % 150 === 0 });
		videoFrame.close();

		if (videoEnc.encodeQueueSize > 2) {
			await videoEnc.flush();
		}
	});

	onRenderStop(async () => {
		console.log('stopping render');
		await videoEnc.flush();
		await audioEnc.flush();
		await waitBlocking();
		muxer.finalize();
		videoEnc.close();
		audioEnc.close();

		console.log('closing fileStream');
		const writer = fileStream.getWriter();
		await writer.close().catch(() => {});
		await fileStream.close().catch(() => {});

		const file = await fileHandle.getFile();
		saveAs(file, "render.webm");
	});
}
