import { EventKey, createEvent } from "vue-three-js";

export const EVENT_FFT: EventKey<{ byteFrequencyData: Uint8Array, byteTimeDomainData: Uint8Array }> = "audio-fft";
export const { useEvent: useFftEvent, onEvent: onFft } = createEvent(EVENT_FFT);
