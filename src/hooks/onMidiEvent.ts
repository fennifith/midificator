import { onScopeDispose } from "vue";
import { MidiEventInfo } from "./createMidiScheduler";
import { useMidiScheduler } from "./useMidiScheduler";
import { unitsToFrames } from "vue-three-js";

export type MidiEventOptions = {
	offset?: string | number;
	debug?: string;
};

export function onMidiEvent(name: string | string[], callback: (event: MidiEventInfo) => void, options: MidiEventOptions = {}) {
	const offset = options.offset && unitsToFrames(options.offset) || undefined;

	const scheduler = useMidiScheduler();
	if (name instanceof Array) {
		for (const event of name)
			scheduler.addEventListener(event, callback, offset);
	} else scheduler.addEventListener(name, callback, offset);

	onScopeDispose(() => {
		scheduler.removeEventListener(callback);
	});

	if (options.debug) {
		function handleDebug(e: KeyboardEvent) {
			if (e.key !== options.debug) return;

			callback({
				name: '',
				value: e.key.charCodeAt(0),
				intensity: 255,
				frame: 0,
			});
		}

		window.addEventListener('keydown', handleDebug);

		onScopeDispose(() => {
			window.removeEventListener('keydown', handleDebug);
		});
	}
}
