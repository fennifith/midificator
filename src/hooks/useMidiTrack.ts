import { Ref, ref, watchEffect } from "vue";
import { ThreeContext, currentFrame, fps, unitsToFrames, useRenderStart, useRenderStop } from "vue-three-js";
import { createMidiScheduler } from "./createMidiScheduler";
import { onBeforeRender } from "vue-three-js";
import { useTrackStore } from "../stores/tracks";
import { useFftEvent } from "./events";

export function useMidiTrack(context: Ref<ThreeContext>, playAudio: boolean = true) {
	const scheduler = createMidiScheduler();
	const trackStore = useTrackStore();

	const trackFrame = ref(0);
	const currentFrameOffset = ref(currentFrame.value);

	const fftEvent = useFftEvent();

	const renderStartEvent = useRenderStart();
	const renderStopEvent = useRenderStop();

	async function dispatchFrame(frame: number) {
		if (!context.value.isRunning) return;

		trackFrame.value = frame - currentFrameOffset.value;

		const currentTrack = trackStore.tracks.find(t => {
			return t.audioStart <= trackFrame.value && t.audioStart + t.audioDuration > trackFrame.value;
		});

		if (currentTrack) {
			const f = trackFrame.value - currentTrack.audioStart;
			const byteFrequencyData = currentTrack.audioFft.byteFrequencyData[f] || new Uint8Array(currentTrack.audioFft.frequencyBinCount).fill(0);
			const byteTimeDomainData = currentTrack.audioFft.byteTimeDomainData[f] || new Uint8Array(currentTrack.audioFft.frequencyBinCount).fill(128);
			fftEvent.emit({ byteFrequencyData, byteTimeDomainData });
		} else {
			fftEvent.emit({
				byteFrequencyData: new Uint8Array([]),
				byteTimeDomainData: new Uint8Array([]),
			});
		}

		for (const t of trackStore.tracks) if (t.audio) {
			const expectedTime = Math.floor((trackFrame.value - t.audioStart) / fps);

			if (playAudio && expectedTime >= 0 && (Number.isNaN(t.audio.duration) || expectedTime < t.audio.duration) && t.audio.paused) {
				t.audio.currentTime = expectedTime;
				await t.audio.play();
			}

			if (expectedTime < 0 || expectedTime > t.audio.duration) {
				t.audio.pause();
			}

			if (playAudio && t.audio.currentTime > 0 && !t.audio.paused) {
				// if the video lags in dev mode, scheduler should skip events to catch up
				const expectedFrame = t.audioStart + unitsToFrames(t.audio.currentTime + "s");
				if (trackFrame.value < expectedFrame) {
					console.log('video delayed - skipping to frame', expectedFrame);
					currentFrameOffset.value += trackFrame.value - expectedFrame;

					// send any frame events that might have been skipped
					for (let i = trackFrame.value + 1; i <= expectedFrame; i++)
						await scheduler.dispatchFrame(i);
				}
			}
		}

		await scheduler.dispatchFrame(trackFrame.value);

		if (trackFrame.value > trackStore.lastFrame) {
			console.log("track over");
			renderStopEvent.emit();
		}
	}

	onBeforeRender(dispatchFrame);

	// pause all audio tracks
	watchEffect(() => {
		for (const track of trackStore.tracks) {
			if (track.audio && !context.value.isRunning)
				track.audio.pause();
		}
	});

	function seekTo(f: number) {
		for (const t of trackStore.tracks) if (t.audio) {
			const expectedTime = (f - t.padStart) / 60;
			if (expectedTime > 0) {
				t.audio.currentTime = expectedTime;
			} else {
				t.audio.pause();
				t.audio.currentTime = 0;
			}
		}

		trackFrame.value = f;
		currentFrameOffset.value = currentFrame.value - trackFrame.value;
	}

	return {
		dispatchFrame, trackFrame,
		play: () => {
			currentFrameOffset.value = currentFrame.value - trackFrame.value;
			renderStartEvent.emit();
		},
		pause: () => {
			renderStopEvent.emit();
		},
		seekTo,
	};
}
